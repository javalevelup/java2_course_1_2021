package org.levelup.threads;

public class ThreadsExample {

    public static void main(String[] args) throws InterruptedException {
        // extends Thread
        // implements Runnable
        // implements Callable

        Thread thread = new PrintThread();
        // thread.run();
        thread.start();  // именно этот метод создает и запускает поток

        Thread countDownThread = new Thread(new CountDown(), "count_down_thread");
        // countDownThread.run();
        countDownThread.start();
        // countDownThread.join(); // останавливает поток main до того момента, как поток count_down_thread не завершится

        Thread daemonThread = new Thread(() -> {
            while (true) { // бесконечный цикл
                printString(String.valueOf(System.currentTimeMillis())); // печатем время в миллисекундах
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException exc) {
                    printString("Interrupted...");
                }
            }
        }, "daemon_thread");
        daemonThread.setDaemon(true);
        daemonThread.start();

        printString("Hello from main");
    }

    static class PrintThread extends Thread {

        @Override
        public void run() {
            // То, что будет делать поток. Как только метод завершится - поток прекратить существование
            printString("Hello from thread");
        }

    }

    static class CountDown implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + " " + i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException exc) {}
            }
        }
    }

    static void printString(String value) {
        System.out.println(Thread.currentThread().getName() + ": " + value);
    }

}
