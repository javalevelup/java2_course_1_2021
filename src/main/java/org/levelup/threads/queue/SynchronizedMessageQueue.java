package org.levelup.threads.queue;

import lombok.SneakyThrows;

import java.util.LinkedList;

public class SynchronizedMessageQueue implements MessageQueue {

    private final LinkedList<MessageTask<?>> queue = new LinkedList<>();
    private final Object fullCondition = new Object();
    private final Object emptyCondition = new Object();
    private final int size; // размер нашей очереди

    public SynchronizedMessageQueue(int size) {
        this.size = size;
    }

    // t1 -> put()
    //  -> берет блокировку у объекта fullCondition
    //  -> очередь заполнена и t1 уходит в ожидаение (в wait())
    //  -> блокировка освобождается
    // Producer
    @Override
    @SneakyThrows
    public <T> void put(MessageTask<T> messageTask) {
        // wait/notify
        synchronized (fullCondition) {
            while (queue.size() >= size) {
                fullCondition.wait();
            }
        }

        synchronized (emptyCondition) {
            queue.addLast(messageTask);
            emptyCondition.notify();
        }
    }

    // Consumer
    @Override
    @SneakyThrows
    public <T> MessageTask<T> take() {
        synchronized (emptyCondition) {
            while (queue.isEmpty()) {
                emptyCondition.wait();
            }
        }

        synchronized (fullCondition) {
            //noinspection unchecked
            MessageTask<T> task = (MessageTask<T>) queue.poll();
            fullCondition.notify(); // переводит поток, который ожидает (в котором был вызван метод wait()), в состояние Runnable
            return task;
        }
    }

}
