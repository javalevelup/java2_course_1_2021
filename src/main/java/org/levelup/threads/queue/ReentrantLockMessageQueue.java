package org.levelup.threads.queue;

import lombok.SneakyThrows;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockMessageQueue implements MessageQueue {

    private final LinkedList<MessageTask<?>> queue = new LinkedList<>();
    private final int size;

    private final ReentrantLock queueLock = new ReentrantLock();
    private final Condition fullCondition = queueLock.newCondition();
    private final Condition emptyCondition = queueLock.newCondition();

    public ReentrantLockMessageQueue(int size) {
        this.size = size;
    }

    @Override
    @SneakyThrows
    public <T> void put(MessageTask<T> messageTask) {
        queueLock.lock();
        try {
            while (queue.size() >= size) {
                fullCondition.await();
            }
            queue.addLast(messageTask);
            emptyCondition.signal();
        } finally {
            queueLock.unlock();
        }
    }

    @Override
    @SneakyThrows
    public <T> MessageTask<T> take() {
        queueLock.lock();
        try {
            while (queue.isEmpty()) {
                emptyCondition.await();
            }
            fullCondition.signal();
            //noinspection unchecked
            return (MessageTask<T>) queue.poll();
        } finally {
            queueLock.unlock();
        }
    }

}
