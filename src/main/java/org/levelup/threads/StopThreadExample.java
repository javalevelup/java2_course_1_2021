package org.levelup.threads;

import lombok.SneakyThrows;

import java.util.List;

public class StopThreadExample {

    private static List<Integer> ids;

    @SneakyThrows
    public static void main(String[] args) {
        Thread thread = new Thread(new Worker(), "worker_thread");
        thread.start();

        // kill -9 pid
        // thread.stop(); << deprecated

        Thread.sleep(100);
        System.out.println("Send first notification to the thread");
        thread.interrupt(); // отправляет уведомление о просьбе завершить поток: устанавливает флаг interrupted в true

        Thread.sleep(100);
        System.out.println("Send second notification to the thread");
        thread.interrupt();

    }

    public static class Worker implements Runnable {
        @Override
        public void run() {
            // Thread.currentThread().isInterrupted() - возвращает флаг interrupted
            // Thread.interrupted() -  возвращает флаг interrupted, но после это очищает этот флаг (возвращает его в false)
            int interruptionCount = 0;
            int value = 1;
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    if (interruptionCount < 1) {
                        System.out.println(Thread.currentThread().getName() + " ignored interruption");
                        interruptionCount++;
                        Thread.interrupted(); // сбросили флаг в false
                    } else {
                        break;
                    }
                }

                System.out.println("Square number: " + (value * value));
                value++;
            }
            System.out.println("Thread finished its work");
        }
    }

}
