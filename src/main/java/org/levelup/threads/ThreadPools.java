package org.levelup.threads;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadPools {


    @SneakyThrows
    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        executorService.execute(() -> {
            System.out.println("Hello from thread from pool");
        });


        // implements Callable
        int pow = 16;
        Future<Integer> futureResult = executorService.submit(() ->{
            int result = 1;
            for (int i = 0; i < 16; i++) {
                result *= 2;
            }
            return result;
        });

        Integer result = futureResult.get(); // get - блокирующий вызов - поток, в котором вызвали метод get, блокируется до получения результата
        System.out.println(result);
        executorService.shutdown();


        System.out.println("Concurrent operations");

        List<Integer> integers = new ArrayList<>(150);
        Random r = new Random();
        for (int i = 0; i < 150; i++) {
            integers.add(r.nextInt(50));
        }

        // [43, 43, 12, 2, 3, .... ] - 50 elements

        ExecutorService pool = Executors.newFixedThreadPool(10);

        List<Future<Integer>> futureThreadResults = new ArrayList<>();
        for (int i = 0; i < integers.size() / 10; i++) {
            // [i*10 ... i * 10 + 9]
            // i = 0 : [0, 9]
            // i = 1 : [10, 19]
            int start = i * 10;
            int end = i * 10 + 9;

            Future<Integer> sum = pool.submit(() -> {
                Thread.sleep(2000);
                int threadResult = 0;
                for (int j = start; j <= end; j++) { // сумму элементов в диапазоне [start, end]
                    threadResult += integers.get(j);
                }
                return threadResult;
            });
            System.out.println("Добавили вычисления в пул (" + start + ", " + end + ")");
            futureThreadResults.add(sum);
        }

        int sum = 0;
        for (Future<Integer> fResult : futureThreadResults) {
            System.out.println("Получение результата вычисления суммы из другого потока: " + System.currentTimeMillis());
            sum += fResult.get();
        }
        System.out.println(sum);


        pool.shutdown();


        // ExecutorService service = Executors.newSingleThreadExecutor(); // создает пул из одного потока
        // ExecutorService service = Executors.newCachedThreadPool(); // создает пул из 0 потоков, но при необходимости создает еще потоки в пуле
//        for (int i = 0; i < 300_000; i++) {
//            service.submit(() -> {
//                Thread.sleep(10_000);
//                return 0;
//            });
//        }

        // ExecutorService cachedPool = new ThreadPoolExecutor(5, 10, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
        ScheduledExecutorService scheduledPool = Executors.newScheduledThreadPool(5);
        // scheduledPool.schedule(() -> System.out.println("Hello world!"), 5, TimeUnit.SECONDS);

        // task1/task2/task3
        // task1 started
        // 5s
        // task2 started
        // 5
        // task3 started
        scheduledPool.scheduleAtFixedRate(() -> System.out.println("Hello world!"), 5, 5, TimeUnit.SECONDS);

        // task1 started
        // task1 finished
        // 5
        // task2 started
        // task2 finished
        scheduledPool.scheduleWithFixedDelay(() -> System.out.println("Hello world!"), 1, 5, TimeUnit.SECONDS);

    }

}
