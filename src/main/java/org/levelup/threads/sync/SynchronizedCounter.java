package org.levelup.threads.sync;

public class SynchronizedCounter implements Counter {

    private final Object mutex = new Object();

    private int value;

    // synchronized
    //   - гарантирует, что только один поток может выполнять метод в единицу времени

    // public synchronized void increment() { value++; }
    @Override
    public void increment() {
        synchronized (this) {
            value++; // value = value + 1
        }
        // value++
        //  1. read value
        //  2. value + 1
        //  3. write value
    }

    @Override
    public int getValue() {
        synchronized (mutex) {
            return value;
        }
    }

}
