package org.levelup.threads.sync;

import java.util.concurrent.atomic.AtomicInteger;

// блокирующий алгоритм (метод) - метод, у которого внутри происходит взятие блокировки
// неблокирующий алгоритм (метод) - lock free/nonblocking algorithm - потокобезопасный метод без взятия блокировок
public class LockFreeCounter implements Counter {

    // CAS - compare and swap (compare and set)
    // value = 43, value++;
    // 1. v = c = memory read - 43
    // 2. c = c + 1 -> 44
    // 3. v1 = memory read
        // if v1 == v -> memory write -> 44
        // if v1 != v -> goto 1p.

    // value -> increment/getValue()

    private AtomicInteger value = new AtomicInteger(0);

    @Override
    public void increment() {
        value.incrementAndGet();
    }

    @Override
    public int getValue() {
        return value.get();
    }

}
