package org.levelup.threads.sync;

import java.util.concurrent.locks.ReentrantLock;

// 3 Типа блокировки
//  - biased
//  - thin
//  - fat
public class ReentrantLockCounter implements Counter {

    private final ReentrantLock counterLock = new ReentrantLock();
    private int value;

    private volatile boolean flag = false;

    public void doWhile() {
        while (!flag) {
            ///....
        }
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public void increment() {
        counterLock.lock(); // взятие блокировки (ожидание взятия блокировки)
        try {
            value++;
        } finally {
            counterLock.unlock();
        }
    }

    @Override
    public int getValue() {
        counterLock.lock();
        try {
            return value;
        } finally {
            counterLock.unlock();
        }
    }

}
