package org.levelup.trello.service.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.trello.model.Board;
import org.levelup.trello.model.User;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HibernateBoardRepositoryTest {

    private SessionFactory factory;
    private Session session;
    private Transaction tx;

    private HibernateBoardRepository boardRepository;

    @BeforeEach
    public void setup() {
        factory = mock(SessionFactory.class);
        session = mock(Session.class);
        tx = mock(Transaction.class);

        when(factory.openSession()).thenReturn(session);
        when(session.beginTransaction()).thenReturn(tx);

        boardRepository = new HibernateBoardRepository(factory);
    }

    @Test
    public void testCreateBoard_allDataIsValid_thenCreateBoard() {
        Integer userId = 103;
        String name = "boardname";

        User user = new User();
        user.setId(userId);

        when(session.load(User.class, userId)).thenReturn(user);

        Board board = boardRepository.createBoard(userId, name, false);
        assertEquals(name, board.getName());
        assertFalse(board.getFavourite());
        assertEquals(userId, board.getOwner().getId());
    }

}
